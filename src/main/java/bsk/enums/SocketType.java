package bsk.enums;

public enum SocketType {
    DATA_FILE("dataFile"),
    DATA_MESSAGE("dataMessage"),
    PUBLIC_KEY("pk"),
    SESSION_KEY("sk");

    private final String type;

    SocketType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
