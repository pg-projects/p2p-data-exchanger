package bsk.client;

import bsk.cybersecurity.SecurityController;
import bsk.server.ConnectionCheckerThread;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class StartupController {
    @FXML private Label infoLabel;
    @FXML private Button connect;

    @FXML public void initialize() {
        ResourceBundle rb = ResourceBundle.getBundle("Config");
        infoLabel.setText("You are waiting for a user on the port " + rb.getString("GUEST_PORT") + "...");
        infoLabel.setTextFill(Color.WHITE);
        connect.setDisable(true);
        new Thread(new ConnectionCheckerThread(connect)).start();
    }

    public void onButtonClicked(ActionEvent event) {
        Object source = event.getSource();
        if (source == connect) {
            try {
                URL url = new File("src/main/resources/ui.fxml").toURI().toURL();
                Parent root = FXMLLoader.load(url);
                Stage stage = new Stage();
                stage.setTitle("p2p data exchanger");
                stage.setScene(new Scene(root, 600, 600, Color.LIGHTGRAY));
                stage.setResizable(false);
                stage.show();
                ((Node)(event.getSource())).getScene().getWindow().hide();
                SecurityController.getInstance().sendPublicKey();
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }
}
