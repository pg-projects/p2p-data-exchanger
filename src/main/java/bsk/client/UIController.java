package bsk.client;

import bsk.cybersecurity.SecurityController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;

import java.io.File;

public class UIController {
    @FXML private Label statusLabel;
    @FXML private ProgressBar progressBar;
    @FXML private Button sendFile;
    @FXML private Button sendMessage;
    @FXML private TextArea message;
    @FXML private RadioButton CBC;

    @FXML public void initialize() {
        message.setFont(Font.font("Verdana", 12));
    }

    public void onButtonClicked(ActionEvent actionEvent) {
        if (SecurityController.getInstance().getGuestPublicKey() != null) {
            if (SecurityController.getInstance().getSessionKey() == null) {
                SecurityController.getInstance().sendSessionKey();
            }
            Object source = actionEvent.getSource();
            if (source == sendFile) {
                FileChooser fileChooser = new FileChooser();
                File selectedFile = fileChooser.showOpenDialog(null);
                if (selectedFile != null) {
                    SendDataTask task;
                    if (CBC.isSelected()) task = new SendDataTask(selectedFile, "CBC");
                    else task = new SendDataTask(selectedFile, "ECB");
                    bindControlsWithTask(task);
                    new Thread(task).start();
                }
            } else if (source == sendMessage) {
                SendDataTask task;
                if (CBC.isSelected()) task = new SendDataTask(message.getText(), "CBC");
                else task = new SendDataTask(message.getText(), "ECB");
                bindControlsWithTask(task);
                new Thread(task).start();
            }
        }
    }

    private void bindControlsWithTask(SendDataTask task) {
        statusLabel.textProperty().bind(task.messageProperty());
        progressBar.progressProperty().bind(task.progressProperty());
    }
}
