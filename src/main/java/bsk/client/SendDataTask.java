package bsk.client;

import bsk.cybersecurity.SecurityController;
import bsk.enums.SocketType;
import javafx.concurrent.Task;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.ResourceBundle;

class SendDataTask extends Task<Void> {
    private final File file;
    private final int bytes;
    private final String message;
    private final String mode;

    SendDataTask(File file, String mode) {
        this.file = file;
        this.bytes = (int) file.length();
        this.message = "";
        this.mode = mode;
    }

    SendDataTask(String message, String mode) {
        this.message = message;
        this.file = null;
        this.bytes = 0;
        this.mode = mode;
    }

    @Override
    protected Void call() throws Exception {
        initiate();
        if (file == null) {
            sendMessage();
        }
        else {
            sendFile();
        }
        finish();
        refresh();
        return null;
    }

    private void initiate() throws InterruptedException {
        updateMessage("Initiating...");
        updateProgress(0, bytes);
        Thread.sleep(1000);
    }

    private void sendFile() {
        updateMessage("Sending...");
        ResourceBundle rb = ResourceBundle.getBundle("Config");
        try (Socket client = new Socket(rb.getString("GUEST_IP"), Integer.parseInt(rb.getString("GUEST_PORT")))) {
            try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
                 DataOutputStream dos = new DataOutputStream(client.getOutputStream())) {
                dos.writeUTF(SocketType.DATA_FILE.getType());
                byte[] buffer = new byte[4096];
                int bytesSent = 0;
                Cipher cipher = setCipher(dos);
                dos.writeUTF(file.getName());
                for (int n = in.read(buffer); n > 0; n = in.read(buffer)) {
                    byte[] outputBytes = cipher.update(buffer, 0, n);
                    dos.write(outputBytes);
                    bytesSent += n;
                    updateProgress(bytesSent, bytes);
                }
                dos.write(cipher.doFinal());
            }
        } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException |
                InvalidKeyException | BadPaddingException | IllegalBlockSizeException |
                InvalidAlgorithmParameterException ex) {
            System.err.println(ex.getMessage());
        }
    }

    private void sendMessage() throws InterruptedException {
        updateMessage("Sending...");
        ResourceBundle rb = ResourceBundle.getBundle("Config");
        try (Socket client = new Socket(rb.getString("GUEST_IP"), Integer.parseInt(rb.getString("GUEST_PORT")))) {
            try (DataOutputStream dos = new DataOutputStream(client.getOutputStream())) {
                dos.writeUTF(SocketType.DATA_MESSAGE.getType());
                Cipher cipher = setCipher(dos);
                byte[] cipherText = cipher.doFinal(message.getBytes());
                dos.writeUTF(Base64.getEncoder().encodeToString(cipherText));
            }
        } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException |
                InvalidKeyException | IllegalBlockSizeException | BadPaddingException |
                InvalidAlgorithmParameterException ex) {
            System.err.println(ex.getMessage());
        }
        Thread.sleep(1000);
    }

    private void finish() throws InterruptedException {
        updateMessage("Finishing...");
        Thread.sleep(1000);
    }

    private void refresh() {
        updateMessage("Operation status!");
        updateProgress(0, 1);
    }

    private Cipher setCipher(DataOutputStream dos) throws IOException, NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidAlgorithmParameterException, InvalidKeyException {
        Cipher cipher;
        if (mode.equals("CBC")) {
            dos.writeUTF("CBC");
            byte[] bytes = new byte[16];
            new SecureRandom().nextBytes(bytes);
            var iv = new IvParameterSpec(bytes);
            dos.writeUTF(Base64.getEncoder().encodeToString(bytes));
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, SecurityController.getInstance().getSessionKey(), iv);
        } else {
            dos.writeUTF("ECB");
            cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, SecurityController.getInstance().getSessionKey());
        }
        return cipher;
    }
}
