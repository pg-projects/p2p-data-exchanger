package bsk.client;

import bsk.cybersecurity.SecurityController;
import bsk.server.WindowMessageCreator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class LoginController {
    @FXML public Button ok;
    @FXML private TextArea login;
    @FXML private PasswordField password;

    public void onButtonClicked(ActionEvent event) {
        if (!login.getText().isEmpty() && !password.getText().isEmpty()) {
            Object source = event.getSource();
            if (source == ok) {
                try {
                    SecurityController.getInstance().generateKeyPair(login.getText(), password.getText());
                    URL url = new File("src/main/resources/startup.fxml").toURI().toURL();
                    Parent root = FXMLLoader.load(url);
                    Stage stage = new Stage();
                    stage.setTitle("p2p data exchanger");
                    stage.setScene(new Scene(root, 450, 150, Color.LIGHTGRAY));
                    stage.setResizable(false);
                    stage.show();
                    ((Node) (event.getSource())).getScene().getWindow().hide();
                } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException |
                        InvalidAlgorithmParameterException | InvalidKeyException | IllegalBlockSizeException |
                        BadPaddingException | InvalidKeySpecException e) {
                    WindowMessageCreator.display("This password is incorrect!");
                }
            }
        } else {
            WindowMessageCreator.display("Password or login can not be empty!");
        }
    }
}
