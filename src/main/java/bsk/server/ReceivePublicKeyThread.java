package bsk.server;

import bsk.cybersecurity.SecurityController;

import java.io.DataInputStream;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

class ReceivePublicKeyThread implements Runnable {
    private final DataInputStream dis;

    ReceivePublicKeyThread(DataInputStream dis) {
        this.dis = dis;
    }

    @Override
    public void run() {
        try {
            byte[] pubKey = dis.readAllBytes();
            X509EncodedKeySpec ks = new X509EncodedKeySpec(pubKey);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PublicKey publicKey = kf.generatePublic(ks);
            SecurityController.getInstance().setGuestPublicKey(publicKey);
        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException ex) {
            System.err.println(ex.getMessage());
        } finally {
            try {
                dis.close();
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            }
        }
    }
}
