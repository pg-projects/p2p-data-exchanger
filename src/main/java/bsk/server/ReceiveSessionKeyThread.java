package bsk.server;

import bsk.cybersecurity.SecurityController;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.DataInputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

class ReceiveSessionKeyThread implements Runnable {
    private final DataInputStream dis;

    ReceiveSessionKeyThread(DataInputStream dis) {
        this.dis = dis;
    }

    @Override
    public void run() {
        try {
            byte[] sessKey = dis.readAllBytes();
            Cipher c = Cipher.getInstance("RSA");
            c.init(Cipher.DECRYPT_MODE, SecurityController.getInstance().getPrivateKey());
            SecurityController.getInstance().setSessionKey(new SecretKeySpec(c.doFinal(sessKey), "AES"));
        } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException |
                InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            System.err.println(ex.getMessage());
        } finally {
            try {
                dis.close();
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            }
        }
    }
}
