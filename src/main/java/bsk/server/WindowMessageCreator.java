package bsk.server;

import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class WindowMessageCreator {

    public static void display(String message) {
        Platform.runLater(() -> {
            VBox layout = createVBoxLayout(message);
            Stage stage = createMessageStage(layout);
            stage.show();
        });
    }

    private static VBox createVBoxLayout(String message) {
        VBox layout = new VBox();
        Label label = new Label(message);
        label.setWrapText(true);
        layout.getChildren().add(label);
        return layout;
    }

    private static Stage createMessageStage(VBox layout) {
        Stage stage = new Stage();
        stage.setTitle("Received message!");
        stage.setScene(new Scene(layout, 300, 250, Color.LIGHTGRAY));
        stage.setResizable(false);
        return stage;
    }

}
