package bsk.server;

import bsk.enums.SocketType;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements Runnable {
    private final ServerSocket server;

    public Server(int port) {
        try {
            this.server = new ServerSocket(port);
            server.setSoTimeout(1000);
        } catch (IOException error) {
            throw new IllegalStateException(error);
        }
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try  {
                Socket socket = server.accept();

                DataInputStream dis = new DataInputStream(socket.getInputStream());
                String type = dis.readUTF();
                if (type.equals(SocketType.DATA_MESSAGE.getType()) || type.equals(SocketType.DATA_FILE.getType())) {
                    new Thread(new ReceiveDataThread(dis, type)).start();
                } else if (type.equals(SocketType.PUBLIC_KEY.getType())) {
                    new Thread(new ReceivePublicKeyThread(dis)).start();
                } else if (type.equals(SocketType.SESSION_KEY.getType())) {
                    new Thread(new ReceiveSessionKeyThread(dis)).start();
                }
            } catch (IOException error) {
                System.err.println(error.getMessage());
            }
        }
        closeServer();
    }

    private void closeServer() {
        try {
            server.close();
        } catch (IOException error) {
            System.err.println(error.getMessage());
        }
    }
}
