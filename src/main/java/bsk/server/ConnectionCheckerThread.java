package bsk.server;

import javafx.fxml.FXML;
import javafx.scene.control.Button;

import java.io.IOException;
import java.net.Socket;
import java.util.ResourceBundle;

public class ConnectionCheckerThread implements Runnable {
    @FXML private Button button;

    private final String guestPORT;
    private final String guestIP;

    public ConnectionCheckerThread(Button button) {
        this.button = button;
        guestPORT = ResourceBundle.getBundle("Config").getString("GUEST_PORT");
        guestIP = ResourceBundle.getBundle("Config").getString("GUEST_IP");
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try (Socket s = new Socket(guestIP, Integer.parseInt(guestPORT))) {
                button.setDisable(false);
                Thread.currentThread().interrupt();
            } catch (IOException e) {
                button.setDisable(true);
            }
        }
    }
}
