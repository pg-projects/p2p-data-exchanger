package bsk.server;

import bsk.cybersecurity.SecurityController;
import bsk.enums.SocketType;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.ResourceBundle;

class ReceiveDataThread implements Runnable {
    private final DataInputStream dis;
    private final String dataType;

    ReceiveDataThread(DataInputStream dis, String dataType) {
        this.dis = dis;
        this.dataType = dataType;
    }

    @Override
    public void run() {
        try {
            Cipher cipher;
            String mode = dis.readUTF();
            if (mode.equals("Acknowledgment")) {
                WindowMessageCreator.display(dis.readUTF());
                return;
            } else if (mode.equals("CBC")) {
                byte[] bytes = Base64.getDecoder().decode(dis.readUTF());
                var iv = new IvParameterSpec(bytes);
                cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                cipher.init(Cipher.DECRYPT_MODE, SecurityController.getInstance().getSessionKey(), iv);
            } else {
                cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                cipher.init(Cipher.DECRYPT_MODE, SecurityController.getInstance().getSessionKey());
            }

            if (dataType.equals(SocketType.DATA_MESSAGE.getType())) {
                byte[] plainText = cipher.doFinal(Base64.getDecoder().decode(dis.readUTF()));
                WindowMessageCreator.display(new String(plainText));
                sendAcknowledgment("Your message was received successfully!");
            } else if (dataType.equals(SocketType.DATA_FILE.getType())) {
                try (BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(dis.readUTF()))) {
                    byte[] buffer = new byte[4096];
                    for (int readBytes = dis.read(buffer); readBytes > -1; readBytes = dis.read(buffer)) {
                        out.write(cipher.update(buffer, 0, readBytes));
                    }
                    out.write(cipher.doFinal());
                    sendAcknowledgment("Your file was received successfully!");
                }
            }
        } catch (IOException | NoSuchPaddingException | IllegalBlockSizeException |
                NoSuchAlgorithmException | BadPaddingException | InvalidKeyException |
                InvalidAlgorithmParameterException ex) {
            System.err.println(ex.getMessage());
        } finally {
            try {
                dis.close();
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    private void sendAcknowledgment(String message) throws IOException {
        ResourceBundle rb = ResourceBundle.getBundle("Config");
        try (Socket client = new Socket(rb.getString("GUEST_IP"), Integer.parseInt(rb.getString("GUEST_PORT")))) {
            try (DataOutputStream dos = new DataOutputStream(client.getOutputStream())) {
                dos.writeUTF(SocketType.DATA_MESSAGE.getType());
                dos.writeUTF("Acknowledgment");
                dos.writeUTF(message);
            }
        }
    }
}
