package bsk.cybersecurity;

import bsk.enums.SocketType;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ResourceBundle;

public class SecurityController {
    private PublicKey publicKey;
    private PrivateKey privateKey;
    private SecretKey sessionKey;
    private PublicKey guestPublicKey;

    private static final SecurityController INSTANCE;

    static {
        SecurityController tmp = null;
        try {
            tmp = new SecurityController();
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            e.printStackTrace();
        }
        INSTANCE = tmp;
    }

    public static SecurityController getInstance() {
        return INSTANCE;
    }

    private SecurityController() throws NoSuchAlgorithmException, NoSuchProviderException {
        if (INSTANCE != null) {
            throw new IllegalStateException("Singleton already constructed.");
        }
    }

    public void sendSessionKey() {
        try {
            sessionKey = new SecretKeySpec(new byte[16], "AES");
            Cipher c = Cipher.getInstance("RSA");
            c.init(Cipher.ENCRYPT_MODE, getGuestPublicKey());
            byte[] encryptedSessionKey = c.doFinal(sessionKey.getEncoded());

            ResourceBundle rb = ResourceBundle.getBundle("Config");
            try (Socket sender = new Socket(rb.getString("GUEST_IP"), Integer.parseInt(rb.getString("GUEST_PORT")))) {
                try (DataOutputStream dos = new DataOutputStream(sender.getOutputStream())) {
                    dos.writeUTF(SocketType.SESSION_KEY.getType());
                    dos.write(encryptedSessionKey);
                }
            }
        } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException |
                InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            System.err.println(e.getMessage());
        }
    }

    public void sendPublicKey() {
        ResourceBundle rb = ResourceBundle.getBundle("Config");
        try (Socket sender = new Socket(rb.getString("GUEST_IP"), Integer.parseInt(rb.getString("GUEST_PORT")))) {
            try (DataOutputStream dos = new DataOutputStream(sender.getOutputStream())) {
                dos.writeUTF(SocketType.PUBLIC_KEY.getType());
                dos.write(publicKey.getEncoded());
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void generateKeyPair(String login, String password) throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidAlgorithmParameterException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
            IOException, InvalidKeySpecException {
        boolean isDirectoryCreated = new File("src/main/resources/" + login).mkdirs();
        if (isDirectoryCreated) {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] hashedPasswordBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));
            SecretKey localKey = new SecretKeySpec(hashedPasswordBytes, "AES");

            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(2048);
            KeyPair kp = kpg.generateKeyPair();
            publicKey = kp.getPublic();
            privateKey = kp.getPrivate();

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] bytes = new byte[16];
            new SecureRandom().nextBytes(bytes);
            cipher.init(Cipher.ENCRYPT_MODE, localKey, new IvParameterSpec(bytes));

            byte[] cipherPublicKey = cipher.doFinal(publicKey.getEncoded());
            byte[] cipherPrivateKey = cipher.doFinal(privateKey.getEncoded());

            createFiles(login, bytes, cipherPublicKey, cipherPrivateKey);
        } else {
            byte[] bytes = Files.readAllBytes(Paths.get("src/main/resources/" + login + "/iv.txt"));
            byte[] cipherPublicKey = Files.readAllBytes(Paths.get("src/main/resources/" + login + "/public.key"));
            byte[] cipherPrivateKey = Files.readAllBytes(Paths.get("src/main/resources/" + login + "/private.key"));

            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] hashedPasswordBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));

            SecretKey localKey = new SecretKeySpec(hashedPasswordBytes, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, localKey, new IvParameterSpec(bytes));
            byte[] publicKeyBytes = cipher.doFinal(cipherPublicKey);
            byte[] privateKeyBytes = cipher.doFinal(cipherPrivateKey);

            KeyFactory kf = KeyFactory.getInstance("RSA");
            publicKey = kf.generatePublic(new X509EncodedKeySpec(publicKeyBytes));
            privateKey = kf.generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
        }
    }

    private void createFiles(String login, byte[] bytes, byte[] cipherPublicKey, byte[] cipherPrivateKey) {
        try (FileOutputStream fos1 = new FileOutputStream("src/main/resources/" + login + "/public.key");
             FileOutputStream fos2 = new FileOutputStream("src/main/resources/" + login + "/private.key");
             FileOutputStream fos3 = new FileOutputStream("src/main/resources/" + login + "/iv.txt")) {
            fos1.write(cipherPublicKey);
            fos2.write(cipherPrivateKey);
            fos3.write(bytes);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public SecretKey getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(SecretKey sessionKey) {
        this.sessionKey = sessionKey;
    }

    public PublicKey getGuestPublicKey() {
        return guestPublicKey;
    }

    public void setGuestPublicKey(PublicKey guestPublicKey) {
        this.guestPublicKey = guestPublicKey;
    }
}
