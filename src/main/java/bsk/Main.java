package bsk;

import bsk.server.Server;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class Main extends Application {
    ResourceBundle rb = ResourceBundle.getBundle("Config");
    private final Thread serverThread = new Thread(new Server(Integer.parseInt(rb.getString("HOST_PORT"))));

    @Override
    public void init() {
        serverThread.start();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL url = new File("src/main/resources/login.fxml").toURI().toURL();
        Parent root = FXMLLoader.load(url);
        primaryStage.setTitle("p2p data exchanger");
        primaryStage.setScene(new Scene(root, 275, 250, Color.LIGHTGRAY));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    @Override
    public void stop() {
        serverThread.interrupt();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
