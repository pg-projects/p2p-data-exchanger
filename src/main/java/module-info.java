module p2p.data.exchanger {
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;
    requires javafx.base;
    requires javafx.media;
    requires javafx.swing;
    requires java.desktop;

    opens bsk;
    opens bsk.server;
    opens bsk.client;
    opens bsk.enums;
}