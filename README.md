## Introduction  
The main task of the project is to design and program an application to cipher
messages and files and then send them by using the Ethernet network. In general,
the application must take a form of a network secure communicational tool. The
application must allow sending and receiving the ciphered data (messages and files)
between both sides of communication. It means that user A have the application,
and so does the user B. Before the transmission user A and B must exchange their
public keys. The pair of RSA keys will be used for a secure session key exchange.
Session key is used to cipher the data. Moreover public and private keys are ciphered
using a local key, that is created based on the password hash, and then stored in files.

## Technologies
- Java 16
- JavaFX